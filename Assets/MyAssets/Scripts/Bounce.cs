﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour {

    AudioSource bounce;
    Rigidbody ballRigidbody;
	void Start () {
        bounce = GetComponent<AudioSource>();
        ballRigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter()
    {
        //Debug.Log(ballRigidbody.velocity.magnitude);
        if (ballRigidbody.velocity.magnitude >= 8f)
        {
            bounce.volume = 1;
        }
        else
        {
            bounce.volume = ballRigidbody.velocity.magnitude / 10;
        }
        bounce.Play();
    }
}
