﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Catcher : MonoBehaviour {

    public float range = 50f;
    public float spaceToLine = 0.01f;
    public float speed = 20f;
    public float dropPower = 2f;
    public float throwPower = 10f;

    Ray catchRay = new Ray();
    RaycastHit catchHit;
    int interactiveMask;
    LineRenderer catchLine;
    Vector3 spaceToLineVector;
    Rigidbody item;


    void Awake()
    {
        interactiveMask = LayerMask.GetMask("Interactive");
        catchLine = GetComponent<LineRenderer>();
        spaceToLineVector.Set(0f, 0f, spaceToLine);
    }

    private void Update()
    {
        LineDrawing();
    }

    private void FixedUpdate()
    {
        Catch();
        Throw();
    }

    private void LineDrawing()
    {
        catchRay.origin = transform.position;
        catchRay.direction = transform.forward;
        catchLine.SetPosition(0, transform.position + spaceToLineVector);
        Physics.Raycast(catchRay, out catchHit, range, interactiveMask);

        if ((catchHit.rigidbody != null) && (!Input.GetButton("Fire1")))
        {
            //Debug.Log("You see: " + catchHit.rigidbody);
            catchLine.SetPosition(1, catchHit.point);
        }
        else
        {
            catchLine.SetPosition(1, catchRay.origin + catchRay.direction * range);
        }

        if ((catchHit.rigidbody != null) && (Input.GetButtonDown("Fire1")))
        {
            //Debug.Log("You touched: " + catchHit.rigidbody);
            item = catchHit.rigidbody;
        }
    }


    private void Catch()
    {
        if (Input.GetButton("Fire1") && item != null)
        {
            item.velocity = Vector3.zero;
            item.angularVelocity = Vector3.zero;
            Vector3 itemNewPosition = (catchLine.GetPosition(1) - item.position ) * speed / item.mass * Time.deltaTime;
            item.MovePosition(item.position + itemNewPosition);
        }
        if(!Input.GetButton("Fire1") && item != null)
        {
            item.velocity = (transform.forward * Input.GetAxis("Mouse Y") * dropPower) 
                + (transform.right * Input.GetAxis("Mouse X") * dropPower);
            item = null;
        }
    }

    private void Throw()
    {
        if (Input.GetButton("Fire2") && item != null)
        {
            item.velocity = transform.forward * throwPower / item.mass;
            item = null;
        }
    }
}
