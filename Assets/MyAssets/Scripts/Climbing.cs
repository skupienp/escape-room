﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climbing : MonoBehaviour {

    public GameObject coin;

    bool got = false;
    bool isPlaying = false;
    float t = 0;


    void OnTriggerEnter(Collider other)
    {
        t = 0;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            t += Time.deltaTime;
            got = coin.GetComponentInChildren<CoinGetter>().got;
            if (isPlaying == false && coin.activeSelf != true && got == false && t >= 3.0f)
            {
                isPlaying = true;
                StartCoroutine(GameManager.WaitAndPlay(1, coin));
            }
        }
    }
}
