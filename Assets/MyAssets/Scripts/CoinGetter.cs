﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinGetter : MonoBehaviour {

    public AudioClip coinGet;
    public GameObject coin;
    [HideInInspector] public bool got = false;
    public Text scoreText;
    public Points points;

    AudioSource audioSource;


    void Start () {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !got)
        {
            StartCoroutine(PlayCoinGet(0.7f));
        }
    }

    private IEnumerator PlayCoinGet(float sec)
    {
        audioSource.clip = coinGet;
        audioSource.Play();
        got = true;
        yield return new WaitForSeconds(sec);
        coin.SetActive(false);
        points.pointsAmount++;
        scoreText.text = points.pointsAmount + "/" + points.maxPoint;
    }
}
