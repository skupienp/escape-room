﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public Text tryAgainText;
    public Text exitGameText;
    public GameObject leaderboardsButton;
    public GameObject backButton;
    public AudioClip coinAppearClip;
    public Animator anim;
    public GameObject scrollView;
    public InputField inputField;

    Text leaderboardsText;
    Text backText;
    Animator backAnim;
    Animator leaderboardsAnim;
    Vector3 vector;

    static AudioClip coinAppear;

    private void Start()
    {
        coinAppear = coinAppearClip;
        leaderboardsText = leaderboardsButton.GetComponentInChildren<Text>();
        backText = backButton.GetComponentInChildren<Text>();
        backAnim = backButton.GetComponent<Animator>();
        leaderboardsAnim = leaderboardsButton.GetComponent<Animator>();
        vector.Set(1.0f, 1.0f, 1.0f);
    }

    private void Update()
    {
        if (Input.GetButton("Cancel"))
        {
            Application.Quit();
        }
    }

    public void Reset()
    {
        if (tryAgainText.color.a == 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void Exit()
    {
        if (exitGameText.color.a == 1)
        {
            Application.Quit();
        }
    }

    public void Leaderboards()
    {
        if (leaderboardsText.color.a == 1)
        {
            anim.SetTrigger("isLeaderboardsPressed");
            backAnim.ResetTrigger("Highlighted");
            backAnim.SetTrigger("Normal");
            backText.rectTransform.localScale = vector;
            leaderboardsButton.SetActive(false);
            backButton.SetActive(true);
            anim.ResetTrigger("isBackPressed");
            scrollView.SetActive(true);
            inputField.interactable = false;
        }
    }

    public void Back()
    {
        if (backText.color.a == 1)
        {
            anim.SetTrigger("isBackPressed");
            leaderboardsAnim.ResetTrigger("Highlighted");
            leaderboardsAnim.SetTrigger("Normal");
            leaderboardsText.rectTransform.localScale = vector;
            backButton.SetActive(false);
            leaderboardsButton.SetActive(true);
            anim.ResetTrigger("isLeaderboardsPressed");
            scrollView.SetActive(false);
        }
    }

    public static IEnumerator WaitAndPlay(float sec, GameObject coin)
    {
        yield return new WaitForSeconds(sec);
        coin.SetActive(true);
        AudioSource pickupAudioSource = coin.GetComponentInChildren<AudioSource>();
        pickupAudioSource.clip = coinAppear;
        pickupAudioSource.Play();
    }
}
