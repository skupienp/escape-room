﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListItem : MonoBehaviour
{

    public Text rank;
    public Text playerName;
    public Text time;

    public void SetItem(string rankText, string nameText, string timeText, bool highlighted)
    {
        rank.text = rankText;
        playerName.text = nameText;
        time.text = timeText;
        if (highlighted == true)
        {
            rank.color = Color.yellow;
            playerName.color = Color.yellow;
            time.color = Color.yellow;
        }
    }
}
