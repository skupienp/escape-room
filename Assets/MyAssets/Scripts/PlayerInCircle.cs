﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInCircle : MonoBehaviour {

    [HideInInspector] public bool isStaying = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            isStaying = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isStaying = false;
        }
    }
}
