﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Points : MonoBehaviour
{

    public int pointsAmount = 0;
    public int maxPoint = 5;
    public GameObject teleportParticles;
    public Canvas canvas;
    public Text timeText;
    public Text noInRankingText;
    public Text scoreText;
    public FirstPersonController firstPersonController;
    public Image cursor;
    public InputField inputField;
    public GameObject inputPlaceholder;
    public GameObject listItemPrefab;
    public GameObject content;

    AudioSource audioSource;
    bool isPlaying = false;
    bool isPlaying2 = false;
    Animator anim;
    MeshRenderer pickup;
    AudioSource playerAudioSource;
    double playTime;
    //readonly string path = "Assets/MyAssets/Texts/Leaderboards.txt";
    readonly string path = "Escape-Room_Data/Leaderboards.txt";
    private List<Player> leaderboards = new List<Player>();
    private int index = 0;
    float t = 0;

    private class Player
    {
        public string Name { get; set; }
        public double Time { get; set; }

        public Player(string Name, double Time)
        {
            this.Name = Name;
            this.Time = Time;
        }
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        anim = canvas.GetComponent<Animator>();
        pickup = canvas.GetComponentInChildren<MeshRenderer>();
        playerAudioSource = firstPersonController.GetComponent<AudioSource>();
        SetLeaderboards();
    }

    private void Update()
    {
        if (pointsAmount == maxPoint && isPlaying == false)
        {
            isPlaying = true;
            teleportParticles.SetActive(true);
            audioSource.Play();
        }
    }


    void OnTriggerEnter(Collider other)
    {
        t = 0;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            t += Time.deltaTime;
            if (pointsAmount == maxPoint && t >= 1.0f && isPlaying2 == false)
            {
                isPlaying2 = true;
                Debug.Log("You win!");
                anim.SetTrigger("isGameWin");
                cursor.enabled = false;
                pickup.enabled = false;
                scoreText.enabled = false;
                teleportParticles.SetActive(false);
                firstPersonController.enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                audioSource.Stop();
                playerAudioSource.mute = true;
                playTime = System.Math.Round(Time.timeSinceLevelLoad, 2);
                timeText.text = "Time: " + playTime;
                if (leaderboards.Count > 0)
                {
                    SetRank();
                }
                int rank = index + 1;
                noInRankingText.text = "No. in ranking: " + rank;
                StartCoroutine(WaitAndInput(2f));
            }
        }
    }

    private void SetLeaderboards()
    {
        StreamReader reader = new StreamReader(path);
        while (!reader.EndOfStream)
        {
            string line = reader.ReadLine();
            string[] nameTime = line.Split(' ');
            double time = System.Convert.ToDouble(nameTime[1]);
            Player player = new Player(nameTime[0], time);
            leaderboards.Add(player);
        }
        reader.Close();
        leaderboards.Sort((x, y) => x.Time.CompareTo(y.Time));
    }

    private void SetRank()
    {
        foreach (Player p in leaderboards)
        {
            if (p.Time > playTime)
            {
                return;
            }
            index++;
        }
    }

    public void WriteToFile(string name)
    {
        inputPlaceholder.SetActive(false);
        Player player = new Player(name, playTime);
        leaderboards.Insert(index, player);
        StreamWriter writer = new StreamWriter(path, false);
        int rank = 0;
        Vector3 y = new Vector3(0f, 0f, 0f);
        foreach (Player p in leaderboards)
        {
            writer.WriteLine(p.Name + " " + p.Time);

            rank++;
            bool highlighted = false;
            if ((index + 1) == rank)
            {
                highlighted = true;
            }
            if(rank <= 12)
            {
                GameObject item = Instantiate(listItemPrefab);
                item.transform.SetParent(content.transform);
                ListItem listItem = item.GetComponent<ListItem>();
                listItem.SetItem(rank.ToString(), p.Name, p.Time.ToString(), highlighted);
                RectTransform itemRectTransform = item.GetComponent<RectTransform>();
                itemRectTransform.anchoredPosition3D = Vector3.zero + y;
                itemRectTransform.localRotation = Quaternion.Euler(Vector3.zero);
                itemRectTransform.localScale = Vector3.one;
                y += new Vector3(0f, -30f, 0f);
            }
        }
        writer.Close();
    }

    private IEnumerator WaitAndInput(float sec)
    {
        yield return new WaitForSeconds(sec);
        inputField.Select();
    }
}
