﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pyramid : MonoBehaviour {

    public Transform box;
    public Transform box1;
    public Transform box2;
    public Transform box3;
    public GameObject coin;

    bool got;
    bool isPlaying = false;
    bool isStaying = false;

    void Update () {
        if((box.position.y > box1.position.y) && (box1.position.y > box2.position.y) && (box2.position.y > box3.position.y) && !Input.GetButton("Fire1"))
        {
            StartCoroutine(Delay(2f));
            got = coin.GetComponentInChildren<CoinGetter>().got;
            if (isStaying == true && coin.activeSelf != true && got == false && isPlaying == false)
            {
                isPlaying = true;
                StartCoroutine(GameManager.WaitAndPlay(1, coin));
            }
        }
    }

    IEnumerator Delay(float sec)
    {
        yield return new WaitForSeconds(sec);
        if ((box.position.y > box1.position.y) && (box1.position.y > box2.position.y) && (box2.position.y > box3.position.y) && !Input.GetButton("Fire1"))
        {
            isStaying = true;
        }
    }
}
