﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public GameObject coin;

    AudioSource audioSource;
    bool got;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            audioSource.Play();
            got = coin.GetComponentInChildren<CoinGetter>().got;
            if (coin.activeSelf != true && got == false)
            {
                StartCoroutine(GameManager.WaitAndPlay(1, coin));
            }
        }
    }
}