﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmashingBoxes : MonoBehaviour
{
    public List<Collider> colliders;
    public List<Rigidbody> smashBoxes;
    public List<Rigidbody> smashBalls;
    public PlayerInCircle playerInCircle;
    public GameObject coin;

    private int ballsCount = 0;
    private bool smashReset = false;
    private bool wrongShoot = false;
    private bool got;
    private Collider ball;
    private List<Position> ballsPositions = new List<Position>();
    private List<Position> boxesPositions = new List<Position>();

    private class Position
    {
        public int Id { get; set; }
        public Vector3 Transform { get; set; }
        public Quaternion Rotation { get; set; }

        public Position(int Id, Vector3 Transform, Quaternion Rotation)
        {
            this.Id = Id;
            this.Transform = Transform;
            this.Rotation = Rotation;
        }
    }

    private void Start()
    {
        foreach (Rigidbody ball in smashBalls)
        {
            Position pos = new Position(ball.GetInstanceID(), ball.transform.position, ball.transform.rotation);
            ballsPositions.Add(pos);

            Collider ballCol = ball.GetComponent<Collider>();

            foreach (Collider col in colliders)
            {

                Physics.IgnoreCollision(ballCol, col);
            }

        }
        foreach (Rigidbody box in smashBoxes)
        {
            Position pos = new Position(box.GetInstanceID(), box.transform.position, box.transform.rotation);
            boxesPositions.Add(pos);

            Collider boxCol = box.GetComponent<Collider>();

            foreach (Collider col in colliders)
            {

                Physics.IgnoreCollision(boxCol, col);
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Smash" && other.gameObject.layer == 9)
        {
            if (playerInCircle.isStaying == true)
            {
                ballsCount++;
            }
            else
            {
                wrongShoot = true;
            }
            Debug.Log("Balls count: " + ballsCount);
            ball = other;
        }
    }

    private void Update()
    {
        if(ball)
        {
            if (ball.attachedRigidbody.velocity.magnitude < 0.4f)
            {
                Rigidbody b = smashBoxes.Find(b1 => b1.transform.position.y > 0.6f);

                if ((b == null || ballsCount == 3 || (wrongShoot == true && !Input.GetButton("Fire1"))) && smashReset == false)
                {
                    smashReset = true;

                    foreach (Rigidbody ball in smashBalls)
                    {
                        Position p = ballsPositions.Find(p1 => p1.Id == ball.GetInstanceID());
                        ball.velocity = Vector3.zero;
                        ball.angularVelocity = Vector3.zero;
                        ball.transform.position = p.Transform;
                        ball.transform.rotation = p.Rotation;
                    }
                    foreach (Rigidbody box in smashBoxes)
                    {
                        Position p = boxesPositions.Find(p1 => p1.Id == box.GetInstanceID());
                        box.velocity = Vector3.zero;
                        box.angularVelocity = Vector3.zero;
                        box.transform.position = p.Transform;
                        box.transform.rotation = p.Rotation;
                    }
                    if (b == null && wrongShoot == false)
                    {
                        got = coin.GetComponentInChildren<CoinGetter>().got;
                        if (coin.activeSelf != true && got == false)
                        {
                            StartCoroutine(GameManager.WaitAndPlay(1, coin));
                        }
                    }
                    smashReset = false;
                    ball = null;
                    wrongShoot = false;
                    ballsCount = 0;
                }
            }
        }
    }
}
